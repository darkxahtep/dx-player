"use strict";

module.exports = function () {
    var createProxy = function (vkUrl) {
        if (!vkUrl) {
            return undefined;
        }

        return "/proxy?url=" + vkUrl;
    };

    return {
        createProxy: createProxy
    };
};
