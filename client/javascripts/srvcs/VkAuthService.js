"use strict";

module.exports = function ($q) {

    var tokenParamsString = localStorage.getItem('vk-token'),
        tokenParams = null;
    if (tokenParamsString !== null) {
        tokenParams = JSON.parse(tokenParamsString);
    }

    var getToken = function () {
        if (tokenParams && tokenParams.expires > new Date().getTime()) {
            return tokenParams.token;
        }
        return null;
    };

    var authorize = function (login, pass) {
        var deferred = $q.defer();

        $.post('/login', { login: login, password: pass }, null, 'json')
            .done(function (data) {
                if (data.error) {
                    deferred.reject(data.error);
                } else {
                    var currentTime = new Date().getTime();
                    var tokenInfo = {
                        token: data.response.access_token,
                        expires: currentTime + (23 * 60 * 60 * 1000),
                        user: data.response.user_id
                    };
                    localStorage.setItem('vk-token', JSON.stringify(tokenInfo));
                    tokenParams = tokenInfo;
                    deferred.resolve(tokenParams.token);
                }
            })
            .fail(function (jqxhr) {
                deferred.reject({ error_code: jqxhr.status, error_msg: jqxhr.statusText });
            });

        return deferred.promise;
    };

    return {
        getToken: getToken,
        authorize: authorize
    }
};