"use strict";

var vaEnded = require('./drctvs/vaEnded'),
    vaProgressFor = require('./drctvs/vaProgressFor'),
    vaPlay = require('./drctvs/vaPlay'),
    vaScrollTop = require('./drctvs/vaScrollTop'),
    vaVolume = require('./drctvs/vaVolume');

module.exports = function (app) {
    app.directive('vaEnded', vaEnded);

    app.directive('vaProgressFor', vaProgressFor);

    app.directive('vaPlay', vaPlay);

    app.directive('vaScrollTop', vaScrollTop);

    app.directive('vaVolume', vaVolume);
};