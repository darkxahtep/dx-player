"use strict";

module.exports = function ($scope, utils, $routeParams, vkAuthService) {
    var returnUri = $routeParams.returnURI;

    $scope.safeApply = utils.safeApply;
    $scope.login = '';
    $scope.password = '';
    $scope.authProgress = false;
    $scope.authorize = function ($event) {
        $event.preventDefault();
        $scope.authProgress = true;
        vkAuthService.authorize($scope.login, $scope.password).then(
            function () {
                utils.redirectTo(returnUri, false);
            },
            function (error) {
                $scope.safeApply(function() {
                    $scope.authProgress = false;
                });
                utils.handleVkError(error);
            }
        );
    }
};