"use strict";

module.exports = function ($scope, utils, vkAudioService, playerService) {
    $scope.safeApply = utils.safeApply;
    $scope.trust = utils.trustHtml;

    var audios = [];
    $scope.visibleAudios = [];

    $scope.loading = true;

    vkAudioService.loadUserAudios().then(
        function (audioList) {
            $scope.safeApply(function () {
                audios = audioList;
                $scope.visibleAudios = audios.slice(0, 80);
                $scope.loading = false;
            })
        },
        function (error) {
            utils.handleVkError(error);
        }
    );

    var infiniteAdd = function (data, target) {
        var firstIndexToAdd = target.length;
        for (var i = 0; i < 30; i++) {
            var current = data[firstIndexToAdd + i];
            if (current) {
                target.push(current);
            }
        }
    };
    $scope.loadMore = function () {
        infiniteAdd(audios, $scope.visibleAudios);
    };

    $scope.player = playerService.getCurrent();

    $scope.playAudio = function (audio, index) {
        $scope.safeApply(function () {
            playerService.setCurrent(index, audio, audios.slice(0, audios.length - 1));
        });
    };

    $scope.formatTime = utils.formatTime;

    $scope.removeFromUserAudios = function (audio, $event) {
        $event.stopPropagation();
        if (confirm("Are you sure that you want to delete audio? This operation can't be undone")) {
            vkAudioService.removeFromUserAudios(audio).then(
                function () {
                    $scope.safeApply(function () {
                        audio.deleted = true;
                    })
                },
                function (error) {
                    utils.handleVkError(error);
                }
            );
        }
    }
};