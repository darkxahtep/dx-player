"use strict";

module.exports = function ($scope, utils, proxyService, playerService, vkAudioService) {
    $scope.safeApply = utils.safeApply;
    $scope.trust = utils.trustHtml;

    $scope.vkProxy = proxyService.createProxy;

    $scope.safeApply(function () {
        $scope.player = playerService.getCurrent();
    });

    $scope.text = '';

    $scope.loadLyrics = function() {
        vkAudioService.loadLyrics($scope.player.current.lyrics_id).then(
            function(text) {
                $scope.safeApply(function() {
                    $scope.text = text;
                });
                $('#lyricsModal').modal({
                    backdrop: false,
                    keyboard: false,
                    show: true
                });
            },
            function(error) {
                utils.handleVkError(error)
            }
        )
    };

    $scope.closeLyricsModal = function() {
        $('#lyricsModal').modal('hide');
    };

    $scope.playNext = function () {
        var index = ++$scope.player.index;
        $scope.safeApply(function () {
            playerService.setCurrent(index);
        });
    };

    $scope.playPrevious = function () {
        var index = --$scope.player.index;
        $scope.safeApply(function () {
            playerService.setCurrent(index);
        });
    };

    $scope.shuffle = function () {
        playerService.shufflePlaylist();
    };
};
