'use strict';

var
    config = require('../config'),
    vkontakte = require('vkontakte'),
    extend = require('../utils/extend');

var vkWrapper  = function(token) {
    var vk = vkontakte(token),
        gOptions = { v: config.get('vk:version') };

    return function(method, options, callback) {
        var vkRequest;
        if(!options) {
            options = {};
        }
        extend(options, gOptions);

        console.log(options);

        if(callback) {
            vkRequest = vk(method, options, callback);
        } else {
            vkRequest = vk(method, options);
        }
        return vkRequest;
    }
};

module.exports = vkWrapper;